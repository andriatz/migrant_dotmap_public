# Migrants dot map - Public documentation

This project contains some files about the *making of* a dot map visualizing deaths of migrants around the world.

Actually there are two versions online:

* dev version: http://138.201.225.72:14000/migrant-map/

* data journalism version: http://migranti.catchy.buzz/

Source of data: Missing migrants project https://missingmigrants.iom.int/
